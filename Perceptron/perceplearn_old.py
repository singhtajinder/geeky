import sys;
from collections import Counter;

inputfile = sys.argv[1];
file1 = open(inputfile, "r");
outputfile = sys.argv[2];

#vocabulary = set();
class_set = set();
weight_dict = { };
dummy_dict = { };
average_percept_dict = { };
error_dict = { };
iterations = 9;
current_iteration = 1;
while(current_iteration < iterations):
	print("current iteration number is:", current_iteration);
	line = file1.readline();
	current_iteration = current_iteration + 1;
	total_predictions = 0.0;
	correct_predictions = 0.0;
	linenumber = 0;
	while(line):
		
#		print("line number :" , linenumber);
		linenumber = linenumber + 1;

		line = line.strip();
		temp_list = line.split();
		generic_class = temp_list[0];
		eachline_dict = {};

		if(generic_class not in class_set):
			class_set.add(generic_class);
			weight_dict[generic_class] = { };
			dummy_dict[generic_class] = { };

		for item in class_set:
			eachline_dict[item] = 0;

		for i in range(1, len(temp_list)):
			for temp_class in class_set:
				if (temp_list[i] not in weight_dict[temp_class]):
					(weight_dict[temp_class])[temp_list[i]] = 0;
					(dummy_dict[temp_class])[temp_list[i]] = 0;
				else:
					eachline_dict[temp_class] = eachline_dict[temp_class] + (weight_dict[temp_class])[temp_list[i]]

		expected_class = max(eachline_dict, key=eachline_dict.get);

		#print("part1 done");

		if(expected_class != generic_class):
			for i in range(1, len(temp_list)):
				(weight_dict[expected_class])[temp_list[i]] =  (weight_dict[expected_class])[temp_list[i]] - 1.0; 
				(weight_dict[generic_class])[temp_list[i]] =  (weight_dict[generic_class])[temp_list[i]] + 1.0; 
#		else:
			#print ("no addition as the classes are same");
#			correct_predictions = correct_predictions + 1.0;
	
#		total_predictions = total_predictions + 1.0;

		line = file1.readline();

	print ("before executing dummy_dict");

	
	for temp_class in class_set:
		for dict_key in weight_dict[temp_class]: # or for dict_key in dummy_dict
			(dummy_dict[temp_class])[dict_key] = (dummy_dict[temp_class])[dict_key] + (weight_dict[temp_class])[dict_key]

	print ("after executing dummy_dict");

#	print("correct", correct_predictions, "total predictions", total_predictions);
#	accuracy = correct_predictions / total_predictions;
#	error_rate = 1 - accuracy;
	
	''' 
	if(error_rate not in error_dict):
		error_dict[error_rate] = weight_dict;

	print("error rate is:", error_rate);
	print("error dictionary is:", error_dict);
	 '''

#	print("accuary is:", accuracy);
#	print("error rate is:", error_rate);
	file1.seek(0);

#minimum_error_rate =  min(error_dict.keys());

#print ("minimum_error_rate is:" , minimum_error_rate);

#store_dict = error_dict[minimum_error_rate];

#print ("before division dictionary is:", dummy_dict);

for temp_class in class_set:
	for dict_key in weight_dict[temp_class]: # or for dict_key in dummy_dict
		(dummy_dict[temp_class])[dict_key] = (dummy_dict[temp_class])[dict_key]/iterations;

#print ("final store dictionary is:", dummy_dict);

file1.close();


file2  =open(outputfile, "w");
file2.write(str(dummy_dict));
file2.close();
