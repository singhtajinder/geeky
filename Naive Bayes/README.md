Name: Tajinder Singh

Steps for Part 1:
1. Use preprocess_train.py to convert TRAINING_DATA to TRAINING_FILE (spam_training.txt)
2. Then execute nblearn.py to create spam.nb (MODEL FILE)
3. Use preprocess_test.py to convert TEST_DATA to test file format.
4. Then execute nbclassify.py to create spam.out using TEST_FILE and MODEL_FILE. syntax is:
     "python3 nbclassify spam.nb TESTFILE > spam.out"


Similar steps for Sentiment analysis.


For MegaM
1. Use megampreprocess.py for preprocessing.
2. Usemegampostprocess_spam.py for post processing of spam output file (spam.megam.output) to get spam.megam.out file
3. Use megampostprocess_sentiment.py for post processing of sentiment output file (sentiment.megam.output) to get sentiment.megam.out file.



# Precision, Recall and F-score on the development data for Naive Bayes classifier for two datasets.

SPAM Dataset:
class: SPAM
precision for class SPAM  is: 0.9587912087912088
recall for class SPAM  is: 0.9614325068870524
F-SCORE for class SPAM  is: 0.9601100412654747
class: HAM
precision for class HAM  is: 0.985985985985986
recall for class HAM  is: 0.985
F-SCORE for class HAM  is: 0.9854927463731865



SENTIMENT Dataset:
class: NEG
precision for class NEG  is: 0.9271947527749748
recall for class NEG  is: 0.9744
F-SCORE for class NEG  is: 0.9501425978739953
class: POS
precision for class POS  is: 0.9667986425339367
recall for class POS  is: 0.9173333333333334
F-SCORE for class POS  is: 0.9413395553115566



MEGAM:
SPAM:
class: HAM
precision for class HAM  is: 0.9266917293233082
recall for class HAM  is: 0.986
F-SCORE for class HAM  is: 0.9554263565891473
class: SPAM
precision for class SPAM  is: 0.9831772575250836
recall for class SPAM  is: 0.8851239669421488
F-SCORE for class SPAM  is: 0.9310271903323263


SENTIMENT:
class: NEG
precision for class NEG  is: 0.9392809587217044
recall for class NEG  is: 0.96402666666666666
F-SCORE for class NEG  is: 0.9497734843437708
class: POS
precision for class POS  is: 0.9399198931909212
recall for class POS  is: 0.9389333333333333
F-SCORE for class POS  is: 0.9394262841894598

