import sys;
import ast;
from sys import stdin;
try:
    import cPickle as pickle
except:
    import pickle


modelfile = sys.argv[1];
file1 = open(modelfile, "rb");
weight_dict = pickle.load(file1);
#weight_dict = file1.readline();
file1.close();

class_set = set();

#weight_dict = ast.literal_eval(weight_dict);

percepclassify_inputlist = list();

for item_key in weight_dict:
	class_set.add(item_key);

line = stdin.readline();
#print ("part1");
while(line):
	line = line.strip();
	line = "startstart/ " + line + " endend/";
	dummy_string = "";
	temp_list = line.split();
#	print("part2");
	for i in range(1, len(temp_list)-1):		
		percepclassify_inputlist.append("prev_" + str(temp_list[i-1]) + " " + str(temp_list[i]) + " " + "next_" + str(temp_list[i+1]));	

	for item in percepclassify_inputlist:
		dummy_list = item.split();

		eachline_dict = { };

		for item_key in class_set:
			eachline_dict[item_key] = 0;

		for temp_word in dummy_list:
			for temp_class in class_set:
				if(temp_word in weight_dict[temp_class]):
					eachline_dict[temp_class] = eachline_dict[temp_class] + (weight_dict[temp_class])[temp_word];

		expected_class = max(eachline_dict, key=eachline_dict.get);
		dummy_string = dummy_string + str(dummy_list[1]) +"/" + str(expected_class) + " ";

	del percepclassify_inputlist[:];
#	print("part3");
	sys.stdout.write(dummy_string + "\n");
	line = stdin.readline();
	
